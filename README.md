# PostalApp

### Описание проекта

PostalApp - веб-приложение, позволяющее отслеживать почтовые отправления.

### Стек технологий

```
IntelliJ IDEA Ultimate
Java SE Development 17
Apache Maven version 4.0.0
Git
Spring Boot
Spring WebMVC
Spring Data JPA
Flyway
Postgresql
Slf4j
Lombok
JUnit 5
Docker
Swagger
Postman
```

### Запуск приложения:

- Клонировать репозиторий
- Выполнить в командной строке `docker compose -f compose.yaml up -d` (Необходимо для запуска БД)
- Запустить проект. 

Также доступна сборка проекта в war-архив через комманду `mvn clean install`

Миграция данных будет осуществлена при помощи Flyway, будут добавлены таблицы и тестовые данные.



### Описание:

Реализованы операции:
- регистрация почтового отправления,
- его прибытие в промежуточное почтовое отделение,
- его убытие из почтового отделения,
- его получение адресатом,
- просмотр статуса и полной истории движения почтового отправления.

Postman коллекция с запросами, демонстрирующими функционал в файле: Postal App.postman_collection.json

Для просмотра списка api приложения добавлен Swagger: http://localhost:8080/swagger-ui/index.html

![img_1.png](img_1.png)

Покрытие тестами составляет 90%

![img.png](img.png)
### Разработчик

```
Асадуллин Эльнур Маратович
Asadelmar@gmail.com
```