package ru.asadullin.postalapp.app.impl.mail;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import ru.asadullin.postalapp.adapter.rest.mail.dto.MailSendingDto;
import ru.asadullin.postalapp.app.api.event.repo.EventRepository;
import ru.asadullin.postalapp.app.api.mail.repo.MailRepository;
import ru.asadullin.postalapp.app.api.office.repo.OfficeRepository;
import ru.asadullin.postalapp.domain.event.Event;
import ru.asadullin.postalapp.domain.mail.MailStatus;
import ru.asadullin.postalapp.domain.mail.MailType;
import ru.asadullin.postalapp.domain.mail.repo.Mail;
import ru.asadullin.postalapp.domain.office.repo.Office;

import java.util.ArrayList;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class SendingToPostalOfficeUseCaseTest {
    private static final String INVALID_MAIL_ID = "There is no mail with id '123'.";
    private static final String INVALID_OFFICE_INDEX = "There is no office with index '123'.";
    private static final MailSendingDto DTO = new MailSendingDto(123, 123);
    private static final Mail MAIL = new Mail(
            123,
            MailType.LETTER,
            "ул. Тестовая 123",
            "Test",
            MailStatus.REGISTERED,
            new Office().setIndex(450105).setName("Office Test"),
            new ArrayList<>());
    private static final Office OFFICE = new Office(
            123,
            "Test",
            "Test st.",
            new ArrayList<>());

    @Mock
    private MailRepository mailRepository;
    @Mock
    private EventRepository eventRepository;
    @Mock
    private OfficeRepository officeRepository;

    @InjectMocks
    private SendingToPostalOfficeUseCase sendingToPostalOfficeUseCase;
    @Captor
    private ArgumentCaptor<Mail> mailArgumentCaptor;
    @Captor
    private ArgumentCaptor<Event> eventArgumentCaptor;

    @Test
    void execute_invalidMailId() {
        when(mailRepository.findById(123)).thenReturn(Optional.empty());
        var ex = assertThrows(IllegalArgumentException.class, () -> sendingToPostalOfficeUseCase.execute(DTO));
        assertEquals(INVALID_MAIL_ID, ex.getMessage());
    }

    @Test
    void execute_invalidOfficeIndex() {
        when(mailRepository.findById(123)).thenReturn(Optional.of(MAIL));
        when(officeRepository.findByIndex(123)).thenReturn(Optional.empty());
        var ex = assertThrows(IllegalArgumentException.class, () -> sendingToPostalOfficeUseCase.execute(DTO));
        assertEquals(INVALID_OFFICE_INDEX, ex.getMessage());
    }

    @Test
    void execute() {
        when(mailRepository.findById(123)).thenReturn(Optional.of(MAIL));
        when(officeRepository.findByIndex(123)).thenReturn(Optional.of(OFFICE));

        sendingToPostalOfficeUseCase.execute(DTO);

        verify(mailRepository, times(1)).save(mailArgumentCaptor.capture());
        verify(eventRepository, times(1)).save(eventArgumentCaptor.capture());
        var mail = mailArgumentCaptor.getValue();
        assertEquals(123, mail.getId());
        assertEquals(123, mail.getIndex().getIndex());
        assertEquals(MailStatus.ON_THE_WAY, mail.getStatus());
        var event = eventArgumentCaptor.getValue();
        assertEquals(mail.getEventMessage(), event.getDescription());
        assertEquals(mail, event.getMail());
    }
}