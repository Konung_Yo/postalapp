package ru.asadullin.postalapp.app.impl.event;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import ru.asadullin.postalapp.app.api.mail.repo.MailRepository;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class GetLastMailStatusUseCaseTest {
    private static final String INVALID_MAIL_ID_EXCEPTION_MESSAGE = "There is no mail with id '123'.";

    @Mock
    private MailRepository mailRepository;

    @InjectMocks
    private GetLastMailStatusUseCase getLastMailStatusUseCase;

    @Test
    void execute_invalidId() {
        when(mailRepository.findById(123)).thenReturn(Optional.empty());
        var ex = assertThrows(IllegalArgumentException.class, () -> getLastMailStatusUseCase.execute(123));
        assertEquals(INVALID_MAIL_ID_EXCEPTION_MESSAGE, ex.getMessage());
    }
}