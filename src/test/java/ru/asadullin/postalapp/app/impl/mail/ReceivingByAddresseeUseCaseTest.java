package ru.asadullin.postalapp.app.impl.mail;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import ru.asadullin.postalapp.app.api.event.repo.EventRepository;
import ru.asadullin.postalapp.app.api.mail.repo.MailRepository;
import ru.asadullin.postalapp.domain.event.Event;
import ru.asadullin.postalapp.domain.mail.MailStatus;
import ru.asadullin.postalapp.domain.mail.MailType;
import ru.asadullin.postalapp.domain.mail.repo.Mail;
import ru.asadullin.postalapp.domain.office.repo.Office;

import java.util.ArrayList;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ReceivingByAddresseeUseCaseTest {
    private static final String INVALID_MAIL_ID_EXCEPTION_MESSAGE = "There is no mail with id '123'.";
    private static final Mail MAIL = new Mail(
            1,
            MailType.LETTER,
            "ул. Тестовая 123",
            "Test",
            MailStatus.IN_POSTAL_OFFICE,
            new Office().setIndex(450105).setName("Office Test"),
            new ArrayList<>());
    @Mock
    private MailRepository mailRepository;
    @Mock
    private EventRepository eventRepository;

    @InjectMocks
    private ReceivingByAddresseeUseCase receivingByAddresseeUseCase;
    @Captor
    private ArgumentCaptor<Mail> mailArgumentCaptor;
    @Captor
    private ArgumentCaptor<Event> eventArgumentCaptor;

    @Test
    void execute_invalidId() {
        when(mailRepository.findById(123)).thenReturn(Optional.empty());
        var ex = assertThrows(IllegalArgumentException.class, () -> receivingByAddresseeUseCase.execute(123));
        assertEquals(INVALID_MAIL_ID_EXCEPTION_MESSAGE, ex.getMessage());
    }

    @Test
    void execute() {
        when(mailRepository.findById(1)).thenReturn(Optional.of(MAIL));

        receivingByAddresseeUseCase.execute(1);

        verify(mailRepository, times(1)).save(mailArgumentCaptor.capture());
        verify(eventRepository, times(1)).save(eventArgumentCaptor.capture());
        var mail = mailArgumentCaptor.getValue();
        assertEquals(1, mail.getId());
        assertEquals(MailStatus.RECEIVED, mail.getStatus());
        var event = eventArgumentCaptor.getValue();
        assertEquals(MAIL.getEventMessage(), event.getDescription());
        assertEquals(mail, event.getMail());
    }
}