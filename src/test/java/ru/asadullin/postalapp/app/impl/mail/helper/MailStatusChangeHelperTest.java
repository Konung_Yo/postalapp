package ru.asadullin.postalapp.app.impl.mail.helper;

import org.junit.jupiter.api.Test;
import ru.asadullin.postalapp.domain.mail.MailStatus;

import static org.junit.jupiter.api.Assertions.*;

class MailStatusChangeHelperTest {

    @Test
    void verifyStatus_registeredStatusToChange() {
        assertTrue(MailStatusChangeHelper.verifyStatus(MailStatus.REGISTERED, MailStatus.ON_THE_WAY));
        assertFalse(MailStatusChangeHelper.verifyStatus(MailStatus.REGISTERED, MailStatus.REGISTERED));
        var ex = assertThrows(IllegalArgumentException.class, () -> MailStatusChangeHelper.verifyStatus(MailStatus.REGISTERED, MailStatus.RECEIVED));
        assertEquals("You can't change status to 'RECEIVED' from status 'REGISTERED'.", ex.getMessage());
    }

    @Test
    void verifyStatus_inPostalOfficeStatusToChange() {
        assertTrue(MailStatusChangeHelper.verifyStatus(MailStatus.IN_POSTAL_OFFICE, MailStatus.ON_THE_WAY));
        assertTrue(MailStatusChangeHelper.verifyStatus(MailStatus.IN_POSTAL_OFFICE, MailStatus.RECEIVED));
        assertFalse(MailStatusChangeHelper.verifyStatus(MailStatus.IN_POSTAL_OFFICE, MailStatus.IN_POSTAL_OFFICE));
        var ex = assertThrows(IllegalArgumentException.class, () -> MailStatusChangeHelper.verifyStatus(MailStatus.IN_POSTAL_OFFICE, MailStatus.REGISTERED));
        assertEquals("You can't change status to 'REGISTERED' from status 'IN_POSTAL_OFFICE'.", ex.getMessage());
    }

    @Test
    void verifyStatus_onTheWayStatusToChange() {
        assertTrue(MailStatusChangeHelper.verifyStatus(MailStatus.ON_THE_WAY, MailStatus.IN_POSTAL_OFFICE));
        assertFalse(MailStatusChangeHelper.verifyStatus(MailStatus.ON_THE_WAY, MailStatus.ON_THE_WAY));
        var ex = assertThrows(IllegalArgumentException.class, () -> MailStatusChangeHelper.verifyStatus(MailStatus.ON_THE_WAY, MailStatus.RECEIVED));
        assertEquals("You can't change status to 'RECEIVED' from status 'ON_THE_WAY'.", ex.getMessage());
    }

    @Test
    void verifyStatus_receivedStatusToChange() {
        assertFalse(MailStatusChangeHelper.verifyStatus(MailStatus.RECEIVED, MailStatus.RECEIVED));
        var ex = assertThrows(IllegalArgumentException.class, () -> MailStatusChangeHelper.verifyStatus(MailStatus.RECEIVED, MailStatus.ON_THE_WAY));
        assertEquals("You can't change status to 'ON_THE_WAY' from status 'RECEIVED'.", ex.getMessage());
    }
}