package ru.asadullin.postalapp;

import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.utility.DockerImageName;

public class TestContainerInitializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {
    private static final String IMAGE_TAG = "postgres:11-alpine";

    @SuppressWarnings("rawtypes")
    public static final PostgreSQLContainer dbContainer;

    static {
        final DockerImageName postgresImage = DockerImageName.parse(IMAGE_TAG)
                .asCompatibleSubstituteFor("postgres");

        dbContainer = new PostgreSQLContainer<>(postgresImage)
                .withDatabaseName("test")
                .withUsername("postgres")
                .withPassword("postgres")
                .withCommand("postgres -N 500")
                .withReuse(true);
        dbContainer.start();
    }

    @Override
    public void initialize(ConfigurableApplicationContext configurableApplicationContext) {
        TestPropertyValues.of(
                        "spring.datasource.url=" + dbContainer.getJdbcUrl(),
                        "spring.datasource.username=" + dbContainer.getUsername(),
                        "spring.datasource.password=" + dbContainer.getPassword())
                .applyTo(configurableApplicationContext.getEnvironment());
    }
}