package ru.asadullin.postalapp.adapter.rest.event;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;
import ru.asadullin.postalapp.IntegrationTestConfiguration;
import ru.asadullin.postalapp.TestSupport;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@IntegrationTestConfiguration
class EventControllerTest extends TestSupport {
    private static final String GET_LAST_MAIL_STATUS_API_PATH = "/api/event/status";
    private static final String GET_MAIL_HISTORY_EVENTS_PATH = "/api/event/history";
    private static final String GET_LAST_MAIL_STATUS_RESPONSE_JSON = "get_last_mail_status_response.json";
    private static final String GET_MAIL_HISTORY_EVENTS_RESPONSE_JSON = "mail_history_events_response.json";

    @Autowired
    private MockMvc mockMvc;

    @Test
    void getLastMailStatus() throws Exception {
        mockMvc.perform(get(GET_LAST_MAIL_STATUS_API_PATH)
                        .param("id", "3"))
                .andExpect(status().isOk())
                .andExpect(content().json(readResourceAsString(GET_LAST_MAIL_STATUS_RESPONSE_JSON)));
    }

    @Test
    void getMailHistoryEvents() throws Exception {
        mockMvc.perform(get(GET_MAIL_HISTORY_EVENTS_PATH)
                        .param("id", "4"))
                .andExpect(status().isOk())
                .andExpect(content().json(readResourceAsString(GET_MAIL_HISTORY_EVENTS_RESPONSE_JSON)));
    }
}