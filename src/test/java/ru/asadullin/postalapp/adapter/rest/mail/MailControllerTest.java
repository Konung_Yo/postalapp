package ru.asadullin.postalapp.adapter.rest.mail;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;
import ru.asadullin.postalapp.IntegrationTestConfiguration;
import ru.asadullin.postalapp.TestSupport;
import ru.asadullin.postalapp.app.api.event.repo.EventRepository;
import ru.asadullin.postalapp.app.api.mail.repo.MailRepository;
import ru.asadullin.postalapp.domain.mail.MailStatus;
import ru.asadullin.postalapp.domain.mail.MailType;

import javax.persistence.EntityManager;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@IntegrationTestConfiguration
class MailControllerTest extends TestSupport {
    private static final String POST_REGISTER_NEW_MAIL_API_PATH = "/api/mail/register";
    private static final String PUT_SENDING_TO_POSTAL_OFFICE_API_PATH = "/api/mail/sending";
    private static final String PUT_RECEIVING_BY_POSTAL_OFFICE_API_PATH = "/api/mail/postal";
    private static final String PUT_RECEIVING_BY_ADDRESSEE_API_PATH = "/api/mail/addressee";
    private static final String POST_REGISTER_NEW_MAIL_REQUEST_JSON = "post_register_new_mail_request.json";
    private static final String PUT_SENDING_TO_POSTAL_OFFICE_REQUEST_JSON = "put_sending_to_postal_office_request.json";

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private MailRepository mailRepository;
    @Autowired
    private EventRepository eventRepository;
    @Autowired
    private EntityManager entityManager;

    @Test
    void registerNewMail() throws Exception {
        mockMvc.perform(post(POST_REGISTER_NEW_MAIL_API_PATH)
                        .contentType(APPLICATION_JSON)
                        .content(readResourceAsString(POST_REGISTER_NEW_MAIL_REQUEST_JSON)))
                .andExpect(status().isOk());

        entityManager.flush();
        entityManager.clear();

        var createdMail = mailRepository.findById(5).orElseThrow();
        assertEquals(MailType.LETTER, createdMail.getType());
        assertEquals(450105, createdMail.getIndex().getIndex());
        assertEquals("109559, г. Москва, ул. Тестовая 123", createdMail.getAddress());
        assertEquals("Тестов Тест Тестович", createdMail.getRecipient());
        var createdEvent = eventRepository.findLastEventByMailId(5);
        assertEquals("Зарегистрирована в почтовом отделении 450105 Отделение Почты России в г. Уфа №105", createdEvent.getDescription());
        assertEquals(5, createdEvent.getMail().getId());
    }

    @Test
    void sendingToPostalOffice() throws Exception {
        mockMvc.perform(put(PUT_SENDING_TO_POSTAL_OFFICE_API_PATH)
                        .contentType(APPLICATION_JSON)
                        .content(readResourceAsString(PUT_SENDING_TO_POSTAL_OFFICE_REQUEST_JSON)))
                .andExpect(status().isOk());

        entityManager.flush();
        entityManager.clear();

        var createdMail = mailRepository.findById(1).orElseThrow();
        assertEquals(109559, createdMail.getIndex().getIndex());
        assertEquals(MailStatus.ON_THE_WAY, createdMail.getStatus());
        var createdEvent = eventRepository.findLastEventByMailId(1);
        assertEquals("Отправлена в почтовое отделение 109559 Отделение Почты России в г. Москва №109559", createdEvent.getDescription());
    }

    @Test
    void receivingByPostalOffice() throws Exception {
        mockMvc.perform(put(PUT_RECEIVING_BY_POSTAL_OFFICE_API_PATH)
                        .param("id", "2"))
                .andExpect(status().isOk());

        entityManager.flush();
        entityManager.clear();

        var createdMail = mailRepository.findById(2).orElseThrow();
        assertEquals(MailStatus.IN_POSTAL_OFFICE, createdMail.getStatus());
        var createdEvent = eventRepository.findLastEventByMailId(2);
        assertEquals("Находится в почтовом отделении 420111 Отделение Почты России в г. Казань №111", createdEvent.getDescription());
    }

    @Test
    void receivingByAddressee() throws Exception {
        mockMvc.perform(put(PUT_RECEIVING_BY_ADDRESSEE_API_PATH)
                        .param("id", "3"))
                .andExpect(status().isOk());

        entityManager.flush();
        entityManager.clear();

        var createdMail = mailRepository.findById(3).orElseThrow();
        assertEquals(MailStatus.RECEIVED, createdMail.getStatus());
        var createdEvent = eventRepository.findLastEventByMailId(3);
        assertEquals("Получена адресатом в почтовом отделении 420111 Отделение Почты России в г. Казань №111", createdEvent.getDescription());

    }
}