package ru.asadullin.postalapp.adapter.persistence.office;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.asadullin.postalapp.domain.office.repo.Office;

public interface OfficeJpaRepository extends JpaRepository<Office, Integer> {
}
