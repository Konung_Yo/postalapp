package ru.asadullin.postalapp.adapter.persistence.office;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;
import ru.asadullin.postalapp.app.api.office.repo.OfficeRepository;
import ru.asadullin.postalapp.domain.office.repo.Office;

import java.util.Optional;

@Repository
@RequiredArgsConstructor
public class OfficeJpaRepositoryAdapter implements OfficeRepository {
    private final OfficeJpaRepository officeJpaRepository;

    @Override
    public Optional<Office> findByIndex(Integer index) {
        return officeJpaRepository.findById(index);
    }
}
