package ru.asadullin.postalapp.adapter.persistence.mail;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.asadullin.postalapp.domain.mail.repo.Mail;

public interface MailJpaRepository extends JpaRepository<Mail, Integer> {
}
