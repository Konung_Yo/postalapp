package ru.asadullin.postalapp.adapter.persistence.event;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.asadullin.postalapp.domain.event.Event;

import java.util.List;

public interface EventJpaRepository extends JpaRepository<Event, Integer> {

    Event findFirstByMailIdOrderByDateTimeDesc(Integer id);

    List<Event> findAllByMailIdOrderByDateTimeAsc(Integer id);
}
