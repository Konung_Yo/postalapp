package ru.asadullin.postalapp.adapter.persistence.mail;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;
import ru.asadullin.postalapp.app.api.mail.repo.MailRepository;
import ru.asadullin.postalapp.domain.mail.repo.Mail;

import java.util.Optional;

@Repository
@RequiredArgsConstructor
public class MailJpaRepositoryAdapter implements MailRepository {
    private final MailJpaRepository mailJpaRepository;

    @Override
    public Mail save(Mail mail) {
        return mailJpaRepository.save(mail);
    }

    @Override
    public Optional<Mail> findById(Integer id) {
        return mailJpaRepository.findById(id);
    }
}
