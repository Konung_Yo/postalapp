package ru.asadullin.postalapp.adapter.persistence.event;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;
import ru.asadullin.postalapp.app.api.event.repo.EventRepository;
import ru.asadullin.postalapp.domain.event.Event;

import java.util.List;

@Repository
@RequiredArgsConstructor
public class EventJpaRepositoryAdapter implements EventRepository {
    private final EventJpaRepository eventJpaRepository;

    @Override
    public void save(Event event) {
        eventJpaRepository.save(event);
    }

    @Override
    public Event findLastEventByMailId(Integer id) {
        return eventJpaRepository.findFirstByMailIdOrderByDateTimeDesc(id);
    }

    @Override
    public List<Event> findAllByMailId(Integer id) {
        return eventJpaRepository.findAllByMailIdOrderByDateTimeAsc(id);
    }
}

