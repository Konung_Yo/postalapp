package ru.asadullin.postalapp.adapter.rest.mail.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
public class MailRegisterDto {
    @NotBlank
    private String type;
    @NotNull
    private Integer index;
    @NotBlank
    private String address;
    @NotBlank
    private String recipient;
}
