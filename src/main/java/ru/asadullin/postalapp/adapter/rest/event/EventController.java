package ru.asadullin.postalapp.adapter.rest.event;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.asadullin.postalapp.adapter.rest.event.dto.EventStatusDto;
import ru.asadullin.postalapp.adapter.rest.event.mapper.EventMapper;
import ru.asadullin.postalapp.app.api.event.GetLastMailStatusInbound;
import ru.asadullin.postalapp.app.api.event.GetMailHistoryEventsInbound;

import java.util.List;

@RestController
@RequestMapping("/api/event")
@RequiredArgsConstructor
@Slf4j
@Tag(name = "API для взаимодействия с событиями")
public class EventController {
    private final GetLastMailStatusInbound getLastMailStatusInbound;
    private final GetMailHistoryEventsInbound getMailHistoryEventsInbound;
    private final EventMapper eventMapper;


    @GetMapping("/status")
    @Operation(summary = "Получение последнего события")
    public ResponseEntity<EventStatusDto> getLastMailStatus(@RequestParam Integer id) {
        LOGGER.info("[registerNewMail];");
        var result = getLastMailStatusInbound.execute(id);
        return ResponseEntity.ok(eventMapper.toEventStatusDto(result));
    }

    @GetMapping("/history")
    @Operation(summary = "Получение истории событий")
    public ResponseEntity<List<EventStatusDto>> getMailHistoryEvents(@RequestParam Integer id) {
        LOGGER.info("[getMailHistoryEvents];");
        var result = getMailHistoryEventsInbound.execute(id);
        return ResponseEntity.ok(eventMapper.toEventStatusListDto(result));
    }
}
