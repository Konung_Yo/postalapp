package ru.asadullin.postalapp.adapter.rest.mail;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.asadullin.postalapp.adapter.rest.mail.dto.MailRegisterDto;
import ru.asadullin.postalapp.adapter.rest.mail.dto.MailSendingDto;
import ru.asadullin.postalapp.app.api.mail.ReceiveByPostalOfficeInbound;
import ru.asadullin.postalapp.app.api.mail.ReceivingByAddresseeInbound;
import ru.asadullin.postalapp.app.api.mail.RegisterNewMailInbound;
import ru.asadullin.postalapp.app.api.mail.SendingToPostalOfficeInbound;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/mail")
@RequiredArgsConstructor
@Slf4j
@Tag(name = "API для взаимодействия с почтовыми отправлениями")
public class MailController {
    private final RegisterNewMailInbound registerNewMailInbound;
    private final ReceiveByPostalOfficeInbound receiveByPostalOfficeInbound;
    private final ReceivingByAddresseeInbound receivingByAddresseeInbound;
    private final SendingToPostalOfficeInbound sendingToPostalOfficeInbound;

    @PostMapping("/register")
    @Operation(summary = "Регистрация нового почтового отправления")
    public ResponseEntity<?> registerNewMail(@Valid @RequestBody MailRegisterDto dto) {
        LOGGER.info("[registerNewMail];");
        registerNewMailInbound.execute(dto);
        return ResponseEntity.ok().build();
    }

    @PutMapping("/sending")
    @Operation(summary = "Убытие из почтового отделения")
    public ResponseEntity<?> sendingToPostalOffice(@Valid @RequestBody MailSendingDto dto) {
        LOGGER.info("[sendingToPostalOffice];");
        sendingToPostalOfficeInbound.execute(dto);
        return ResponseEntity.ok().build();
    }

    @PutMapping("/postal")
    @Operation(summary = "Получение отправления почтовым офисом")
    public ResponseEntity<?> receivingByPostalOffice(@RequestParam Integer id) {
        LOGGER.info("[receivingByPostalOffice];");
        receiveByPostalOfficeInbound.execute(id);
        return ResponseEntity.ok().build();
    }

    @PutMapping("/addressee")
    @Operation(summary = "Получение отправления адресатом")
    public ResponseEntity<?> receivingByAddressee(@RequestParam Integer id) {
        LOGGER.info("[receivingByAddressee];");
        receivingByAddresseeInbound.execute(id);
        return ResponseEntity.ok().build();
    }
}
