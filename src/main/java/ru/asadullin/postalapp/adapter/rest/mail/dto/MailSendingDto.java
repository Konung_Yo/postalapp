package ru.asadullin.postalapp.adapter.rest.mail.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
public class MailSendingDto {
    @NotNull
    private Integer id;
    @NotNull
    private Integer index;
}
