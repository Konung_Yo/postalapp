package ru.asadullin.postalapp.adapter.rest.event.mapper;

import org.mapstruct.Mapper;
import ru.asadullin.postalapp.adapter.rest.event.dto.EventStatusDto;
import ru.asadullin.postalapp.domain.event.Event;

import java.util.List;

@Mapper(componentModel = "spring")
public interface EventMapper {
    List<EventStatusDto> toEventStatusListDto(List<Event> result);

    EventStatusDto toEventStatusDto(Event result);
}
