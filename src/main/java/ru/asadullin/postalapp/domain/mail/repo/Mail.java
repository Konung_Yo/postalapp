package ru.asadullin.postalapp.domain.mail.repo;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.asadullin.postalapp.domain.event.Event;
import ru.asadullin.postalapp.domain.mail.MailStatus;
import ru.asadullin.postalapp.domain.mail.MailType;
import ru.asadullin.postalapp.domain.office.repo.Office;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@Table(name = "mail")
public class Mail {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(name = "type", nullable = false)
    @Enumerated(EnumType.STRING)
    private MailType type;
    @Column(name = "address", nullable = false)
    private String address;
    @Column(name = "recipient", nullable = false)
    private String recipient;
    @Column(name = "status", nullable = false)
    @Enumerated(EnumType.STRING)
    private MailStatus status;

    @ManyToOne
    @JoinColumn(name = "office_index")
    private Office index;
    @OneToMany(mappedBy = "mail")
    private List<Event> events;

    public String getEventMessage() {
        return String.join(" ", List.of(status.getDescription(), index.getIndex().toString(), index.getName()));
    }
}
