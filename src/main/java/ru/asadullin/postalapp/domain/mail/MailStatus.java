package ru.asadullin.postalapp.domain.mail;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum MailStatus {
    REGISTERED("Зарегистрирована в почтовом отделении"),
    IN_POSTAL_OFFICE("Находится в почтовом отделении"),
    ON_THE_WAY("Отправлена в почтовое отделение"),
    RECEIVED("Получена адресатом в почтовом отделении");
    private final String description;
}
