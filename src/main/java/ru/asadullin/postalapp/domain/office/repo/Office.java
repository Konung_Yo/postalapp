package ru.asadullin.postalapp.domain.office.repo;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.asadullin.postalapp.domain.mail.repo.Mail;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@Table(name = "office")
public class Office {
    @Id
    private Integer index;
    @Column(name = "name", nullable = false)
    private String name;
    @Column(name = "address", nullable = false)
    private String address;

    @OneToMany(mappedBy = "index")
    private List<Mail> mails;
}
