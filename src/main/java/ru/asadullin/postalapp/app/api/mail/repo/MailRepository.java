package ru.asadullin.postalapp.app.api.mail.repo;

import ru.asadullin.postalapp.domain.mail.repo.Mail;

import java.util.Optional;

public interface MailRepository {
    Mail save(Mail mail);

    Optional<Mail> findById(Integer id);
}
