package ru.asadullin.postalapp.app.api.event.repo;

import ru.asadullin.postalapp.domain.event.Event;

import java.util.List;

public interface EventRepository {
    void save(Event event);

    Event findLastEventByMailId(Integer id);

    List<Event> findAllByMailId(Integer id);
}
