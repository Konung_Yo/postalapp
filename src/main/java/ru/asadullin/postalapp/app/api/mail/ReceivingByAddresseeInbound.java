package ru.asadullin.postalapp.app.api.mail;

public interface ReceivingByAddresseeInbound {
    void execute(Integer id);
}
