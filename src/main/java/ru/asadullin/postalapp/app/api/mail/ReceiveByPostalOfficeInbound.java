package ru.asadullin.postalapp.app.api.mail;

public interface ReceiveByPostalOfficeInbound {
    void execute(Integer id);
}
