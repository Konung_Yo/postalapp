package ru.asadullin.postalapp.app.api.office.repo;

import ru.asadullin.postalapp.domain.office.repo.Office;

import java.util.Optional;

public interface OfficeRepository {
    Optional<Office> findByIndex(Integer index);
}
