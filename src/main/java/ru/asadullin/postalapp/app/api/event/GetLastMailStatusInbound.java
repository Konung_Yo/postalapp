package ru.asadullin.postalapp.app.api.event;

import ru.asadullin.postalapp.domain.event.Event;

public interface GetLastMailStatusInbound {
    Event execute(Integer id);
}
