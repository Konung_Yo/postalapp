package ru.asadullin.postalapp.app.api.mail;

import ru.asadullin.postalapp.adapter.rest.mail.dto.MailRegisterDto;

public interface RegisterNewMailInbound {
    void execute(MailRegisterDto dto);
}
