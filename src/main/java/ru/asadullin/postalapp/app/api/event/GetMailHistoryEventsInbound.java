package ru.asadullin.postalapp.app.api.event;

import ru.asadullin.postalapp.domain.event.Event;

import java.util.List;

public interface GetMailHistoryEventsInbound {
    List<Event> execute(Integer id);
}
