package ru.asadullin.postalapp.app.api.mail;

import ru.asadullin.postalapp.adapter.rest.mail.dto.MailSendingDto;

public interface SendingToPostalOfficeInbound {
    void execute(MailSendingDto dto);
}
