package ru.asadullin.postalapp.app.impl.mail;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import ru.asadullin.postalapp.adapter.rest.mail.dto.MailRegisterDto;
import ru.asadullin.postalapp.app.api.event.repo.EventRepository;
import ru.asadullin.postalapp.app.api.mail.RegisterNewMailInbound;
import ru.asadullin.postalapp.app.api.mail.repo.MailRepository;
import ru.asadullin.postalapp.app.api.office.repo.OfficeRepository;
import ru.asadullin.postalapp.domain.event.Event;
import ru.asadullin.postalapp.domain.mail.MailStatus;
import ru.asadullin.postalapp.domain.mail.MailType;
import ru.asadullin.postalapp.domain.mail.repo.Mail;

import java.time.LocalDateTime;

import static java.lang.String.format;

@Component
@RequiredArgsConstructor
@Transactional
public class RegisterNewMailUseCase implements RegisterNewMailInbound {
    private static final String INVALID_OFFICE_INDEX = "There is no office with index '%s'.";

    private final MailRepository mailRepository;
    private final EventRepository eventRepository;
    private final OfficeRepository officeRepository;

    @Override
    public void execute(MailRegisterDto dto) {
        var office = officeRepository.findByIndex(dto.getIndex())
                .orElseThrow(() -> new IllegalArgumentException(format(INVALID_OFFICE_INDEX, dto.getIndex())));

        var mail = mailRepository.save(new Mail()
                .setType(MailType.valueOf(dto.getType()))
                .setAddress(dto.getAddress())
                .setRecipient(dto.getRecipient())
                .setStatus(MailStatus.REGISTERED)
                .setIndex(office));

        eventRepository.save(new Event()
                .setDescription(mail.getEventMessage())
                .setDateTime(LocalDateTime.now())
                .setMail(mail));
    }
}
