package ru.asadullin.postalapp.app.impl.mail;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import ru.asadullin.postalapp.adapter.rest.mail.dto.MailSendingDto;
import ru.asadullin.postalapp.app.api.event.repo.EventRepository;
import ru.asadullin.postalapp.app.api.mail.SendingToPostalOfficeInbound;
import ru.asadullin.postalapp.app.api.mail.repo.MailRepository;
import ru.asadullin.postalapp.app.api.office.repo.OfficeRepository;
import ru.asadullin.postalapp.app.impl.mail.helper.MailStatusChangeHelper;
import ru.asadullin.postalapp.domain.event.Event;
import ru.asadullin.postalapp.domain.mail.MailStatus;

import java.time.LocalDateTime;

import static java.lang.String.format;

@Component
@RequiredArgsConstructor
@Transactional
public class SendingToPostalOfficeUseCase implements SendingToPostalOfficeInbound {
    private static final String INVALID_MAIL_ID = "There is no mail with id '%s'.";
    private static final String INVALID_OFFICE_INDEX = "There is no office with index '%s'.";

    private final OfficeRepository officeRepository;
    private final MailRepository mailRepository;
    private final EventRepository eventRepository;

    @Override
    public void execute(MailSendingDto dto) {
        var mail = mailRepository.findById(dto.getId())
                .orElseThrow(() -> new IllegalArgumentException(format(INVALID_MAIL_ID, dto.getId())));
        var office = officeRepository.findByIndex(dto.getIndex())
                .orElseThrow(() -> new IllegalArgumentException(format(INVALID_OFFICE_INDEX, dto.getIndex())));

        if (MailStatusChangeHelper.verifyStatus(mail.getStatus(), MailStatus.ON_THE_WAY)) {
            mailRepository.save(mail
                    .setIndex(office)
                    .setStatus(MailStatus.ON_THE_WAY));

            eventRepository.save(new Event()
                    .setDescription(mail.getEventMessage())
                    .setDateTime(LocalDateTime.now())
                    .setMail(mail));
        }
    }
}
