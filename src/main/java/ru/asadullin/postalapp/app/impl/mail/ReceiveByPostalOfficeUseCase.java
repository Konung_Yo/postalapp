package ru.asadullin.postalapp.app.impl.mail;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import ru.asadullin.postalapp.app.api.event.repo.EventRepository;
import ru.asadullin.postalapp.app.api.mail.ReceiveByPostalOfficeInbound;
import ru.asadullin.postalapp.app.api.mail.repo.MailRepository;
import ru.asadullin.postalapp.app.impl.mail.helper.MailStatusChangeHelper;
import ru.asadullin.postalapp.domain.event.Event;
import ru.asadullin.postalapp.domain.mail.MailStatus;

import java.time.LocalDateTime;

import static java.lang.String.format;

@Component
@RequiredArgsConstructor
@Transactional
public class ReceiveByPostalOfficeUseCase implements ReceiveByPostalOfficeInbound {
    private static final String INVALID_MAIL_ID = "There is no mail with id '%s'.";

    private final MailRepository mailRepository;
    private final EventRepository eventRepository;

    @Override
    public void execute(Integer id) {
        var mail = mailRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException(format(INVALID_MAIL_ID, id)));

        if (MailStatusChangeHelper.verifyStatus(mail.getStatus(), MailStatus.IN_POSTAL_OFFICE)) {
            mailRepository.save(mail.setStatus(MailStatus.IN_POSTAL_OFFICE));
            eventRepository.save(new Event()
                    .setDescription(mail.getEventMessage())
                    .setDateTime(LocalDateTime.now())
                    .setMail(mail));
        }
    }
}
