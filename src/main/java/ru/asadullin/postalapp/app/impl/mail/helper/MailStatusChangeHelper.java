package ru.asadullin.postalapp.app.impl.mail.helper;

import ru.asadullin.postalapp.domain.mail.MailStatus;

import static java.lang.String.format;

public class MailStatusChangeHelper {
    private static final String INVALID_STATUS_CHANGE = "You can't change status to '%s' from status '%s'.";

    public static boolean verifyStatus(MailStatus statusToChange, MailStatus statusFromDto) {
        return switch (statusToChange) {
            case REGISTERED -> registeredStatus(statusFromDto);
            case IN_POSTAL_OFFICE -> inPostalOfficeStatus(statusFromDto);
            case ON_THE_WAY -> onTheWayStatus(statusFromDto);
            case RECEIVED -> receivedStatus(statusFromDto);
        };

    }

    private static boolean registeredStatus(MailStatus mailDtoStatus) {
        if (mailDtoStatus.equals(MailStatus.ON_THE_WAY)) {
            return true;
        } else if (mailDtoStatus.equals(MailStatus.REGISTERED)) {
            return false;
        } else {
            throw new IllegalArgumentException(format(INVALID_STATUS_CHANGE, mailDtoStatus, MailStatus.REGISTERED));
        }
    }

    private static boolean inPostalOfficeStatus(MailStatus mailDtoStatus) {
        if (mailDtoStatus.equals(MailStatus.ON_THE_WAY) || mailDtoStatus.equals(MailStatus.RECEIVED)) {
            return true;
        } else if (mailDtoStatus.equals(MailStatus.IN_POSTAL_OFFICE)) {
            return false;
        } else {
            throw new IllegalArgumentException(format(INVALID_STATUS_CHANGE, mailDtoStatus, MailStatus.IN_POSTAL_OFFICE));
        }
    }


    private static boolean onTheWayStatus(MailStatus mailDtoStatus) {
        if (mailDtoStatus.equals(MailStatus.IN_POSTAL_OFFICE)) {
            return true;
        } else if (mailDtoStatus.equals(MailStatus.ON_THE_WAY)) {
            return false;
        } else {
            throw new IllegalArgumentException(format(INVALID_STATUS_CHANGE, mailDtoStatus, MailStatus.ON_THE_WAY));
        }
    }

    private static boolean receivedStatus(MailStatus mailDtoStatus) {
        if (mailDtoStatus.equals(MailStatus.RECEIVED)) {
            return false;
        } else {
            throw new IllegalArgumentException(format(INVALID_STATUS_CHANGE, mailDtoStatus, MailStatus.RECEIVED));
        }
    }
}
