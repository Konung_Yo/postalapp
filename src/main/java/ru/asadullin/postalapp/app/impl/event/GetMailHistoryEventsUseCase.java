package ru.asadullin.postalapp.app.impl.event;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import ru.asadullin.postalapp.app.api.event.GetMailHistoryEventsInbound;
import ru.asadullin.postalapp.app.api.event.repo.EventRepository;
import ru.asadullin.postalapp.app.api.mail.repo.MailRepository;
import ru.asadullin.postalapp.domain.event.Event;

import java.util.List;

import static java.lang.String.format;

@Component
@RequiredArgsConstructor
@Transactional
public class GetMailHistoryEventsUseCase implements GetMailHistoryEventsInbound {
    private static final String INVALID_MAIL_ID = "There is no mail with id '%s'.";

    private final MailRepository mailRepository;
    private final EventRepository eventRepository;

    @Override
    public List<Event> execute(Integer id) {
        mailRepository.findById(id).orElseThrow(() -> new IllegalArgumentException(format(INVALID_MAIL_ID, id)));
        return eventRepository.findAllByMailId(id);
    }

}
