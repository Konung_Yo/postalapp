package ru.asadullin.postalapp.fw.spring;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConversionException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.http.HttpStatus.BAD_REQUEST;

@RestControllerAdvice(basePackages = "ru.asadullin.postalapp.adapter.rest")
@RequiredArgsConstructor
@Slf4j
public class HttpExceptionHandler {
    @ExceptionHandler({IllegalArgumentException.class, IllegalStateException.class, HttpMessageConversionException.class})
    public ResponseEntity<String> handleClientErrors(Exception ex) {
        LOGGER.info("Handled exception :", ex);
        return new ResponseEntity<>(ex.getMessage(), BAD_REQUEST);
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<String> handleValidationExceptions(MethodArgumentNotValidException ex) {
        LOGGER.info("Handled exception :", ex);
        List<String> blankFields = new ArrayList<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            blankFields.add(fieldName);
        });
        String message = "Next fields must not be blank or null: " + String.join(", ", blankFields);
        return new ResponseEntity<>(message, BAD_REQUEST);
    }
}
