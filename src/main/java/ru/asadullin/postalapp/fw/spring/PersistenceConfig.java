package ru.asadullin.postalapp.fw.spring;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories("ru.asadullin.postalapp.adapter.persistence")
@EntityScan({"ru.asadullin.postalapp.adapter.persistence", "ru.asadullin.postalapp.domain"})
public class PersistenceConfig {
}
