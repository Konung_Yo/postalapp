package ru.asadullin.postalapp.fw.spring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = "ru.asadullin.postalapp")
public class PostalApplication extends SpringBootServletInitializer {


    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return builder.sources(PostalApplication.class);
    }

    public static void main(String[] args) {
        SpringApplication sa = new SpringApplication(PostalApplication.class);
        sa.run(args);
    }

}
